'use strict'

var should = require('chai').should();
var chai = require('chai');
var expect = require('chai').expect;
let yargs = require('yargs')
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
chai.use(sinonChai);
var controller = require('../controller/controller');

describe('command tests', () => {
    var stub;
    beforeEach(() => {
      yargs.reset()
      stub = sinon.stub(controller);
    })

    afterEach(() => {
        stub.list.restore();
        stub.insert.restore();
        stub.update.restore();
        stub.remove.restore();
        stub.getOne.restore();
    })

    describe('list command', ()=>{
        it('missing argument command', done=>{
            yargs([])
            .demand(1)
            .fail((msg) => {
              msg.should.equal('Not enough non-option arguments: got 0, need at least 1')
              return done()
            }).parse()
        });

        it('argument command not on the list is provided', done=>{
           let argv = yargs(['list']).command('list').parse();
           expect(argv._).to.be.an('array');
           argv._.should.include('list');
           return done();
        });

        it('get notes', done=>{
            let argv = yargs(['list']).command('list').parse();

            controller.list()

            expect(controller.list).to.have.been.calledOnce;
            return done();
         });

    })

    describe('add command', ()=>{
        it('missing argument command', done=>{
            yargs([])
            .demand(1)
            .fail((msg) => {
              msg.should.equal('Not enough non-option arguments: got 0, need at least 1')
              return done()
            }).parse()
        });

        it('argument command not on the list is provided', done=>{
           let argv = yargs().command('add').parse("add --title='Agile' --body='Spring A'");
           expect(argv._).to.be.an('array');
           argv._.should.include('add');
           expect(argv.title).to.be.a('string');
           expect(argv.body).to.be.a('string');
           return done();
        });

        it('add a note', done=>{
            let argv = yargs().command('add').parse("add --title='Agile' --body='Spring A'");
            let obj = {
                title: argv.title,
                body: argv.body
            }
            controller.insert(obj);
            expect(controller.insert).to.be.calledOnce;

            return done();
         });
    })

    describe('read command', ()=>{
        it('missing argument command', done=>{
            yargs([])
            .demand(1)
            .fail((msg) => {
              msg.should.equal('Not enough non-option arguments: got 0, need at least 1')
              return done()
            }).parse()
        });

        it('argument command not on the list is provided', done=>{
           let argv = yargs().command('read').parse("read --title='Agile'");
           expect(argv._).to.be.an('array');
           argv._.should.include('read');
           expect(argv.title).to.be.a('string');
           
           return done();
        });

        it('it should read a note', done=>{
            let argv = yargs().command('add').parse("add --title='Agile'");
            
            controller.getOne(argv.title);
            expect(controller.getOne).to.be.calledOnce;
            
            return done();
         });
    })

    describe('remove command', ()=>{
        it('missing argument command', done=>{
            yargs([])
            .demand(1)
            .fail((msg) => {
              msg.should.equal('Not enough non-option arguments: got 0, need at least 1')
              return done()
            }).parse()
        });

        it('argument command not on the list is provided', done=>{
           let argv = yargs().command('remove').parse("remove --title='Agile'");
           expect(argv._).to.be.an('array');
           argv._.should.include('remove');
           expect(argv.title).to.be.a('string');
           return done();
        });

        it('it should remove a note', done=>{
            let argv = yargs().command('remove').parse("remove --title='Agile'");

            controller.remove(argv.title);
            expect(controller.remove).to.be.calledOnce;

            return done();
         });
    })

    describe('update command', ()=>{
        it('missing argument command', done=>{
            yargs([])
            .demand(1)
            .fail((msg) => {
              msg.should.equal('Not enough non-option arguments: got 0, need at least 1')
              return done()
            }).parse()
        });

        it('argument command not on the list is provided', done=>{
           let argv = yargs().command('update').parse("update --title='Agile' --body='body to be updated'");
           expect(argv._).to.be.an('array');
           argv._.should.include('update');
           expect(argv.title).to.be.a('string');
           expect(argv.body).to.be.a('string');
           return done();
        });

        it('it should update a note', done=>{
            let argv = yargs().command('update').parse("update --title='Agile' --body='body to be updated'");
            
            let obj = {
                title: argv.title,
                body: argv.body
            }
            controller.update(obj);
            expect(controller.update).to.be.calledOnce;

            return done();
         });
    })
})