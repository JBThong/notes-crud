
var {annoucement} = require('../annoucement');
var {pipeData, pipeReadItem} = require('../helper/common')
var { getAll,
    insertRecord, 
    updateRecord, 
    readRecord, 
    deleteRecord, 
    findRecord,
    indexOfRecord 
} = require('../models/crud');

var {COLORS_RES} = require('../variable');
var _ = require('lodash');

function list(){
    console.log(annoucement('Your notes', COLORS_RES.INFO));
    console.log(pipeData(getAll()));
}

function insert(info){
    if(!_.isEmpty(findRecord(info.title))){
        console.log(annoucement("Note title taken!", COLORS_RES.ERROR));
        return
    }
    if(insertRecord(info)){
        console.log(annoucement("New note added!", COLORS_RES.SUCCESS));
    }
}

function update(data){
    let index = indexOfRecord(data.title);
    if(index === -1)
    {
        console.log(annoucement("Note not found!", COLORS_RES.ERROR));
        return
    }
    if(updateRecord(data,index)){
        console.log(annoucement("Note is updated", COLORS_RES.SUCCESS));
    }
}

function remove(title){
    if(_.isEmpty(findRecord(title))){
        console.log(annoucement("Note not found!", COLORS_RES.ERROR))
    }
    else{
        if(deleteRecord(title)){
            console.log(annoucement("Note removed!", COLORS_RES.SUCCESS));
        }
    }
}

function getOne(title){
    let result = readRecord(title);
    !_.isEmpty(result) ? console.log(pipeReadItem(result)) : console.log(annoucement("Note not found!", COLORS_RES.ERROR));
}
module.exports = {
    list,
    insert,
    update,
    remove,
    getOne
}