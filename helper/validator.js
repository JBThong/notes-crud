'use-strict'
var {CMD_LIST} = require('../variable');
var _ = require('lodash');

function validateCmd(argv){
    for(let i = 0;i < CMD_LIST.length;i++){
        if(CMD_LIST[i] === argv[0]){
            //command not correct options
            if(argv.length > 1){
                console.log("See 'node app.js " +  argv[0].toString() +  " ' --help.")
                return false
            }
            return true;
        }
    }
    //command not found
    console.log(argv[0].toString() + ' is not a node app.js command')
    console.log("See 'node app.js --help'")
    return false
}

function validateFlag(key, value){
    if(_.isUndefined(value)){
        console.log('Not enough non-option arguments');
        return false;
    }
    if(!_.isString(value)){
        console.log("The '" + key +"' just have a value");
        return false;
    }
    return true
}

module.exports = {
    validateCmd,
    validateFlag
}